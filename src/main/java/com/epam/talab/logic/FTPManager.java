package com.epam.talab.logic;


import com.epam.talab.dao.FTPProvider;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class FTPManager {


    public static final String USERPROFILE = "USERPROFILE";

    //printing folder content
    public static void printContent() {
        try {
            System.out.println("\n" + "Current dir: " + FTPProvider.getClient().printWorkingDirectory());
            String[] fileNames = FTPProvider.getClient().listNames();
            System.out.println("Name recieved");
            for (String name : fileNames) {
                System.out.println(name);
            }
        } catch (IOException ex) {
            System.out.println("Can't get files list " + ex.getMessage());
        }
    }


    //navigation - upper directory
    public static void upper() {
        try {
            String path = FTPProvider.getClient().printWorkingDirectory();
            int i = path.lastIndexOf("/");

            if (i > 0) {
                path = path.substring(0, i);
                FTPProvider.getClient().changeWorkingDirectory(path);
                printContent();

            }
            else if (i == 0 && path.length() > 1){
                FTPProvider.getClient().changeWorkingDirectory("/");
                printContent();
            }
            else {
                System.out.println("You are in the root folder!");
                printContent();
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }


    //disconnection
    public static void disconnect() throws IOException {
        FTPProvider.getClient().logout();
        FTPProvider.getClient().disconnect();
        System.out.println("You have been logged out and disconnected.");
    }


    //connection
    public static void connect(String server, String login, String password) {
        try {
            FTPProvider.getClient().connect(server);
            FTPProvider.getClient().login(login, password);
            //ftpClient.enterLocalPassiveMode();

            System.out.println("Connected to " + server + ".");
            System.out.println(FTPProvider.getClient().getReplyString());
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }


    //downloading file
    public static String downloadFile(String destination, String fileName) {
        if (destination.length() == 0 || destination.equalsIgnoreCase("default")) {
            destination = System.getenv(USERPROFILE) + "\\Downloads";
        }
        try {
            File fileDwnld = new File(destination + File.separator + fileName);
            FileOutputStream fileoutputstream = new FileOutputStream(fileDwnld);
            FTPProvider.getClient().retrieveFile(fileName, fileoutputstream);
            fileoutputstream.close();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        return destination;
    }


    //getting user's input
    public static String getInput(String request) {
        String input = "";
        System.out.println("\n" + request);

        try {
            input = FTPProvider.getReader().readLine();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        return input;
    }


}

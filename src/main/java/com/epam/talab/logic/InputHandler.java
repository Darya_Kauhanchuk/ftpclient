package com.epam.talab.logic;


import com.epam.talab.dao.FTPProvider;
import org.apache.commons.net.ftp.FTPFile;


import java.io.IOException;

public class InputHandler {

    //Input + handling
    public static void handleInput(String request) {
        try {
            String input = FTPManager.getInput(request);

            if (input.equalsIgnoreCase("disconnect")) {
                FTPManager.disconnect();

            } else if (input.equalsIgnoreCase("up")) {
                FTPManager.upper();
                handleInput("Enter command:");

            } else {
                FTPFile[] filesAndDirectories = FTPProvider.getClient().listFiles();
                boolean foundFileOrDir = false;
                for (FTPFile fileOrDir : filesAndDirectories) {
                    if (fileOrDir.getName().equals(input)) {
                        //check if enter is a folder
                        if (fileOrDir.isDirectory()) {
                            foundFileOrDir = true;
                            FTPProvider.getClient().changeWorkingDirectory(FTPProvider.getClient().printWorkingDirectory()
                                    + "/" + input);
                            FTPManager.printContent();
                            handleInput("Enter command:");
                            //check if enter is a file
                        } else if (fileOrDir.isFile()) {
                            foundFileOrDir = true;
                            input = FTPManager.getInput("Starting download file " + fileOrDir.getName() + "?(y/n)");
                            if (input.equalsIgnoreCase("y")) {
                                //downloading file
                                String destination = FTPManager.getInput("Enter destination directory:");
                                destination = FTPManager.downloadFile(destination, fileOrDir.getName());
                                input = FTPManager.getInput("File " + fileOrDir.getName() + " downloaded into "
                                        + destination + ".");
                            }
                        }
                        handleInput("Enter command:");
                        break;
                    }
                }
                //if file or directory does not exist
                if (!foundFileOrDir) {
                    handleInput("File or directory not found. Enter another file or directory:");
                }
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
}

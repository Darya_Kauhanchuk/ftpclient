package com.epam.talab.dao;


import org.apache.commons.net.ftp.FTPClient;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class FTPProvider {

    private static FTPClient ftpClient;
    private static BufferedReader reader;

    public static FTPClient getClient() {
        if (ftpClient == null) {
            ftpClient = new FTPClient();
        }
        return ftpClient;
    }

    public static BufferedReader getReader() {

        if (reader == null) {
            InputStreamReader isr = new InputStreamReader(System.in);
            reader = new BufferedReader(isr);
        }
        return reader;
    }
}

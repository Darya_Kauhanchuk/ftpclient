package com.epam.talab;

import com.epam.talab.dao.FTPProvider;
import com.epam.talab.logic.FTPManager;
import com.epam.talab.logic.InputHandler;

public class Main {

    public static void main(String[] args) {


        //Getting data for connection
        String server;
        String login;
        String password;
        server = FTPManager.getInput("Enter FTP server:");
        if (!server.equalsIgnoreCase("default")) {
            login = FTPManager.getInput("Enter login:");
            password = FTPManager.getInput("Enter password:");
        } else {
            server = "ftp.mozilla.org";
            login = "anonymous";
            password = "";
        }


        //Connect and logon to FTP server
        FTPManager.connect(server, login, password);

        //Printing root folder content
        FTPManager.printContent();

        //Dialog
        InputHandler.handleInput("Enter file or directory:");
    }
}








